﻿namespace NUnitSample
{
    /// <summary>
    /// Класс для реализации простых математических операций
    /// </summary>
    public class Math
    {
        /// <summary>
        /// Операция вычитания
        /// </summary>
        /// <param name="a">Уменьшаемое</param>
        /// <param name="b">Вычитаемое</param>
        /// <returns>Результат вычитания</returns>
        public double Substring(double a, double b)
        {
            return a - b;
        }

        /// <summary>
        /// Операция умножения
        /// </summary>
        /// <param name="a">Множимое</param>
        /// <param name="b">Множитель</param>
        /// <returns>Результат умножения</returns>
        public double Multiple(double a, double b)
        {
            return a * b;
        }
    }
}
