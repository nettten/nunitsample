﻿using NUnit.Framework;

namespace NUnitSample.Test
{
    [Author("Avetisyan S.")]
    [TestFixture]
    public class MathTests
    {
        static object[] substringTestsCase =
        {
            new[] { 2.5, 1.2, 1.3 },
            new[] { -10, 1.5, -11.5 },
            new[] { double.MaxValue, double.MaxValue, 0 },
            new[] { -12, -1, -11 },
            new[] { 0, 0, 0 }
        };

        [Test(Description = "Тесты для операции вычитания", Author = "Автора так же можно указать здесь")]
        [Author("Ivanov I.")]
        [TestCaseSource(nameof(substringTestsCase))]
        public void SubstringTests(double a, double b, double expected)
        {
            Math math = new Math();
            double actual = math.Substring(a, b);

            Assert.That(Is.Equals(expected, actual));
        }

        [Test(Description = "Тесты для операции умножения"), Sequential]
        [Author("...")]
        public void MultipleTests(
            [Values(0, 1, 2, 3, 4, 5)] double a, 
            [Values(5, 4, 3, 2, 1, 0)] double b, 
            [Values(0, 4, 6, 6, 4, 0)] double expected)
        {
            Math math = new Math();
            double actual = math.Multiple(a, b);

            Assert.That(actual.Equals(expected), Is.True);
        }
    }
}
